import Vue from 'vue'
import App from './App.vue'
import './rem'
import router from './router'

import { Button,Form ,FormItem,Input,Pagination} from 'element-ui';

Vue.prototype.$ELEMENT = { size: 'small', zIndex: 3000 };
Vue.use(Button).use(Form).use(FormItem).use(Input).use(Pagination)

// router.beforeEach((to, from, next) => {
//   console.log(to,from);
//   next()
// })

// 设置浏览器没有提示
Vue.config.productionTip = false


// new Vue({
//   render: h => h(App),
//   router
// }).$mount('#app')
new Vue({
  router,
  render: h => h(App),
}).$mount('#app')
