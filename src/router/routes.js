// 路由动态加载
const Home = () => import('../pages/Home.vue')
const Page = ()=> import('../pages/Page.vue')
export default [
  {
    path: '/home/:id',
    component: Home,
    meta: {
      name:'tom'
    },
    props: route => ({path:route.path})
  },
  {
    path: '/page',
    component: Page,
    props: {
      msg:100
    }
  },
  {
    path: '/',
    redirect: '/page'
  }
]